/**
movies challenge
*/
package main

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {

	mongoUrl := "127.0.0.1"
	database := "movies"

	session, dbErr := mgo.Dial(mongoUrl)
	if dbErr != nil {
		panic(dbErr)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)

	m := Movies{session.DB(database), "movies"}
	r := httprouter.New()
	r.GET("/movies", m.findAllMovies)
	r.GET("/movies/:title", m.findMovieByName)
	r.PUT("/movies/:title", m.addMovie)

	log.Printf("Starting /movies on port 9090")
	log.Fatal(http.ListenAndServe(":9090", Log(r)))
}

func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func formatResponseJSON(w http.ResponseWriter, content interface{}) {
	w.Header().Add("Content-Type", "application/json")
	output, err := json.Marshal(content)
	if err != nil {
		http.Error(w, "Error formatting content (json).", http.StatusInternalServerError)
	}
	_, err = w.Write(output)
	if err != nil {
		http.Error(w, "Error writting response body (json).", http.StatusInternalServerError)
	}
}

type Movies struct {
	Db         *mgo.Database
	Collection string
}

type Movie struct {
	Id          bson.ObjectId `bson:"_id" json:"-"`
	Title       string        `json:"title"`
	ReleaseDate string        `json:"releaseDate"`
	Producer    string        `json:"producer"`
}

func (m Movies) findAllMovies(rw http.ResponseWriter, req *http.Request, params httprouter.Params) {
	var results = []Movie{}
	err := m.Db.C(m.Collection).Find(nil).All(&results)
	if err != nil {
		panic(err)
	}
	formatResponseJSON(rw, results)
}

// Returns a single movie that matches the given name.
func (m Movies) findMovieByName(rw http.ResponseWriter, req *http.Request, params httprouter.Params) {
	var result Movie

	title := params.ByName("title")

	err := m.Db.C(m.Collection).Find(bson.M{"title": &bson.RegEx{Pattern: "^" + title + "$", Options: "i"}}).One(&result)

	if err != nil {
		if err == mgo.ErrNotFound {
			http.Error(rw, "No movie matching the title was found", http.StatusNotFound)
		}
		panic(err)
	}

	formatResponseJSON(rw, result)
}

func (m Movies) addMovie(rw http.ResponseWriter, req *http.Request, params httprouter.Params) {
	title := params.ByName("title")

	if req.Body == nil {
		http.Error(rw, "Request must contain a valid payload.", http.StatusBadRequest)
		panic("No Content")
	}

	defer req.Body.Close()
	content, err := ioutil.ReadAll(req.Body)

	if err != nil || string(content) == "" {
		http.Error(rw, "Expected content in body.", http.StatusBadRequest)
		panic(err)
	}

	var requestMovie Movie
	err = json.Unmarshal(content, &requestMovie)

	//	if strings.ToLower(title) != strings.ToLower(requestMovie.Title) {
	//		http.Error(rw, "Title mismatch. URL title property does not match content title property.", http.StatusBadRequest)
	//		return
	//	}

	if err != nil {
		http.Error(rw, "Invalid movie data.", http.StatusBadRequest)
		panic(err)
	}

	var existing Movie
	err = m.Db.C(m.Collection).Find(bson.M{"title": &bson.RegEx{Pattern: "^" + title + "$", Options: "i"}}).One(&existing)

	if err == mgo.ErrNotFound {
		requestMovie.Id = bson.NewObjectId()
		err = m.Db.C(m.Collection).Insert(requestMovie)
		if err != nil {
			http.Error(rw, "Error adding movie to database", http.StatusInternalServerError)
			panic(err)
		}
		formatResponseJSON(rw, requestMovie)
		return
	}

	update := requestMovie
	update.Id = existing.Id
	log.Print(update)
	err = m.Db.C(m.Collection).UpdateId(existing.Id, update)
	if err != nil {
		log.Print(err)
		panic(err)
	}

	formatResponseJSON(rw, update)
}
