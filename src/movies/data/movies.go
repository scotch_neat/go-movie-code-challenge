package data

//import (
//	"gopkg.in/mgo.v2"
//)

//type Movies struct {
//	Db         *mgo.Database
//	Collection string
//}
//
//type Movie struct {
//	Title       string `bson:"title"`
//	ReleaseDate string `bson:"releaseDate"`
//	Producer    string `bson:"producer"`
//}
//
//func (m Movies) findAllMovies(rw http.ResponseWriter, req *http.Request, params httprouter.Params) {
//	var results = []Movie{}
//	err := m.Db.C(m.Collection).Find(nil).All(&results)
//	if err != nil {
//		panic(err)
//	}
//	formatResponseJSON(rw, results)
//}
//
//// Returns a single movie that matches the given name.
//func (m Movies) findMovieByName(rw http.ResponseWriter, req *http.Request, params httprouter.Params) {
//	if params.ByName("title") == "" {
//		http.Error(rw, "Missing parameter: title", http.StatusBadRequest)
//	}
//
//	var result Movie
//
//	title := params.ByName("title")
//
//	err := m.Db.C(m.Collection).Find(bson.M{"title": &bson.RegEx{Pattern: title, Options: "i"}}).One(&result)
//
//	if err != nil {
//		if err == mgo.ErrNotFound {
//			http.Error(rw, "No movie matching the title was found", http.StatusNotFound)
//			return
//		}
//		panic(err)
//	}
//
//	formatResponseJSON(rw, result)
//}
//
//func (m Movies) addMovie(rw http.ResponseWriter, req *http.Request, params httprouter.Params) {
//
//	if req.Body == nil {
//		http.Error(rw, "Request must contain a valid payload.", http.StatusBadRequest)
//	}
//
//	defer req.Body.Close()
//	content, bodyErr := ioutil.ReadAll(req.Body)
//
//	if bodyErr != nil || string(content) == "" {
//		panic(bodyErr)
//	}
//
//	var movie Movie
//	marshalErr := json.Unmarshal(content, &movie)
//
//	if marshalErr != nil {
//		panic(marshalErr)
//	}
//
//	insertErr := m.Db.C(m.Collection).Insert(movie)
//
//	if insertErr != nil {
//		http.Error(rw, "Error adding movie to database", http.StatusInternalServerError)
//	}
//	formatResponseJSON(rw, movie)
//}
